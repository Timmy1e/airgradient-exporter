use std::sync::atomic::AtomicU64;

use prometheus_client::metrics::family::Family;
use prometheus_client::metrics::gauge::Gauge;

use crate::Labels;

type BoxGaugeU64 = Box<Family<Labels, Gauge<u64, AtomicU64>>>;
type BoxGaugeF64 = Box<Family<Labels, Gauge<f64, AtomicU64>>>;

#[derive(Clone)]
pub struct Metrics {
    pub wifi: BoxGaugeF64,
    pub temperature: BoxGaugeF64,
    pub carbon_dioxide: BoxGaugeU64,
    pub relative_humidity: BoxGaugeU64,
    pub particulate_matter: BoxGaugeU64,
    pub total_volatile_organic_compounds: BoxGaugeF64,
}

impl Default for Metrics {
    fn default() -> Metrics {
        Metrics {
            wifi: Box::new(Family::<Labels, Gauge<f64, AtomicU64>>::default()),
            temperature: Box::new(Family::<Labels, Gauge<f64, AtomicU64>>::default()),
            carbon_dioxide: Box::new(Family::<Labels, Gauge<u64, AtomicU64>>::default()),
            relative_humidity: Box::new(Family::<Labels, Gauge<u64, AtomicU64>>::default()),
            particulate_matter: Box::new(Family::<Labels, Gauge<u64, AtomicU64>>::default()),
            total_volatile_organic_compounds: Box::new(
                Family::<Labels, Gauge<f64, AtomicU64>>::default(),
            ),
        }
    }
}
