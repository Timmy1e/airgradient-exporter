use clap::Parser;
use log::LevelFilter;

#[derive(Parser)]
#[clap(author, version, about)]
pub struct Args {
    /// Address to bind the server to
    #[clap(short, long, env, default_value = "0.0.0.0")]
    pub address: String,

    /// Port to listen on
    #[clap(short, long, env, default_value = "3000")]
    pub port: u16,

    /// Log level
    #[clap(
        short,
        long,
        env = "RUST_LOG",
        parse(try_from_str),
        default_value = "info"
    )]
    pub log_level: LevelFilter,
}
