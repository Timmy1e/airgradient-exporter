use prometheus_client::encoding::text::Encode;

#[derive(Clone, Debug, Hash, PartialEq, Eq, Encode)]
pub struct Labels {
    sensor_id: String,
}

impl Labels {
    pub fn new(sensor_id: String) -> Labels {
        Labels { sensor_id }
    }
}
