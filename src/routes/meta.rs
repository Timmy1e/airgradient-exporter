use actix_web::{get, HttpResponse, Responder};

use crate::helpers::error::IntoHttpError;
use crate::helpers::error_text;
use crate::{METRICS, REGISTRY};

/// Process GET requests for the health endpoint
#[get("/meta/health")]
pub async fn get_health() -> actix_web::Result<impl Responder> {
    // Try to lock the Arcs, to check if everything is OK
    std::mem::drop(
        METRICS
            .lock()
            .http_internal_error(error_text::ERROR_COULD_NOT_LOCK_METRICS)?,
    );
    std::mem::drop(
        REGISTRY
            .lock()
            .http_internal_error(error_text::ERROR_COULD_NOT_LOCK_REGISTRY)?,
    );

    // All checks passed, so return an OK
    Ok(HttpResponse::Ok())
}

/// Static `robots.txt` "file"
#[get("/robots.txt")]
pub async fn get_robots() -> &'static str {
    "User-agent: *\n\
    Disallow: /"
}
