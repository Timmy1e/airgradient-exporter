use actix_web::http::StatusCode;
use actix_web::{get, HttpResponseBuilder, Responder};
use prometheus_client::encoding::text;

use crate::helpers::error::IntoHttpError;
use crate::helpers::error_text;
use crate::REGISTRY;

/// Process GET requests for the metrics endpoint
#[get("/metrics")]
pub async fn get_metrics() -> actix_web::Result<impl Responder> {
    // Get a lock on the registry
    let registry = &REGISTRY
        .lock()
        .http_internal_error(error_text::ERROR_COULD_NOT_LOCK_REGISTRY)?;

    // Convert the registry to a byte vector
    let mut encoded = Vec::new();
    text::encode(&mut encoded, registry)
        .http_internal_error(error_text::ERROR_COULD_NOT_ENCODE_REGISTRY)?;

    // Create a result with the byte vector
    let result = HttpResponseBuilder::new(StatusCode::OK)
        .content_type("application/openmetrics-text; version=1.0.0; charset=utf-8")
        .body(encoded);

    // Return the result
    Ok(result)
}
