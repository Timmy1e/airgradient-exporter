use std::collections::HashMap;

use actix_web::{post, web, HttpResponse, Responder};
use log::debug;

use crate::helpers::error::IntoHttpError;
use crate::helpers::error_text;
use crate::models::Labels;
use crate::METRICS;

/// Process POST requests to the measures endpoint
#[post("/sensors/airgradient:{id}/measures")]
pub async fn post_measures(
    id: web::Path<String>,
    data: web::Json<HashMap<String, f64>>,
) -> actix_web::Result<impl Responder> {
    let id = id.into_inner();
    let labels = Labels::new(id.to_ascii_lowercase());

    // Get a lock on the metrics
    let metrics = METRICS
        .lock()
        .http_internal_error(error_text::ERROR_COULD_NOT_LOCK_METRICS)?;
    debug!("Sensor {} gave: {:?}", id, data.keys());

    // Wifi strength
    if let Some(&value) = data.get("wifi") {
        debug!("Sensor {} gave 'wifi': {}", id, value);
        metrics.wifi.get_or_create(&labels).set(value);
    }

    // Temperature
    if let Some(&value) = data.get("atmp") {
        debug!("Sensor {} gave 'atmp': {}", id, value);
        metrics.temperature.get_or_create(&labels).set(value);
    }

    // Total Volatile Organic Compounds
    if let Some(&value) = data.get("tvoc") {
        debug!("Sensor {} gave 'tvoc': {}", id, value);
        metrics
            .total_volatile_organic_compounds
            .get_or_create(&labels)
            .set(value);
    }

    // PM2.5 particulate matter
    if let Some(&value) = data.get("pm02") {
        debug!("Sensor {} gave 'pm02': {}f64", id, value);
        if let Some(value) = f64_to_u64(value) {
            debug!("Sensor {} gave 'pm02': {}u64", id, value);
            metrics.particulate_matter.get_or_create(&labels).set(value);
        }
    }

    // CO2
    if let Some(&value) = data.get("rco2") {
        debug!("Sensor {} gave 'rco2': {}f64", id, value);
        if let Some(value) = f64_to_u64(value) {
            debug!("Sensor {} gave 'rco2': {}u64", id, value);
            metrics.carbon_dioxide.get_or_create(&labels).set(value);
        }
    }

    // Relative humidity
    if let Some(&value) = data.get("rhum") {
        debug!("Sensor {} gave 'rhum': {}f64", id, value);
        if let Some(value) = f64_to_u64(value) {
            debug!("Sensor {} gave 'rhum': {}u64", id, value);
            metrics.relative_humidity.get_or_create(&labels).set(value);
        }
    }

    // Just respond with a created
    Ok(HttpResponse::Created())
}

/// Convert a float to a unsigned int, if it's less then 0, return a None
fn f64_to_u64(float: f64) -> Option<u64> {
    if float < 0f64 {
        None
    } else {
        Some(float.round() as u64)
    }
}
