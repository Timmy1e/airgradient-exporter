#[macro_use]
extern crate lazy_static;

use std::ops::DerefMut;
use std::sync::{Arc, Mutex};

use actix_web::{middleware::Logger, App, HttpServer};
use clap::Parser;
use prometheus_client::registry::Registry;
use simple_logger::SimpleLogger;

use crate::models::{Args, Labels, Metrics};
use crate::routes::{get_metrics, meta, post_measures};

mod helpers;
mod models;
mod routes;

lazy_static! {
    static ref ARGS: Args = Args::parse();
    static ref METRICS: Arc<Mutex<Metrics>> = Arc::new(Mutex::new(Metrics::default()));
    static ref REGISTRY: Arc<Mutex<Registry>> = Arc::new(Mutex::new(Registry::default()));
}

/// Main entrypoint
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Create a new Simple Logger, that uses the args log_level as default
    SimpleLogger::new()
        .with_level(ARGS.log_level)
        .env()
        .init()
        .unwrap();

    // Get a lock on the registry and metrics, register the metrics
    if let Ok(mut registry) = REGISTRY.lock() {
        if let Ok(metrics) = METRICS.lock() {
            helpers::registry::register(registry.deref_mut(), &metrics);
        }
    } else {
        panic!("Couldn't get a lock the registry!");
    }

    // Start the server
    HttpServer::new(|| {
        App::new()
            // Add the logger
            .wrap(Logger::default())
            // Set the main endpoints
            .service(post_measures)
            .service(get_metrics)
            // Add some meta endpoints
            .service(meta::get_health)
            .service(meta::get_robots)
    })
    .bind((ARGS.address.clone(), ARGS.port))?
    .run()
    .await
}
