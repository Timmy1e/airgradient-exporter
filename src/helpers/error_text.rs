pub static ERROR_COULD_NOT_ENCODE_REGISTRY: &str = "Couldn't encode registry";
pub static ERROR_COULD_NOT_LOCK_REGISTRY: &str = "Couldn't get lock on registry";
pub static ERROR_COULD_NOT_LOCK_METRICS: &str = "Couldn't get lock on metrics";
