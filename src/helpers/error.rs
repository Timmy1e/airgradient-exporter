use std::error::Error;

use actix_web::{error::InternalError, http::StatusCode};
use log::error;

/// Trait to convert regular errors to HTTP errors.
/// From [this Reddit post](https://www.reddit.com/r/rust/comments/ozc0m8/an_actixanyhow_compatible_error_helper_i_found/).
pub trait IntoHttpError<T> {
    fn http_error(
        self,
        message: &str,
        status_code: StatusCode,
    ) -> core::result::Result<T, actix_web::Error>;

    fn http_internal_error(self, message: &str) -> core::result::Result<T, actix_web::Error>
    where
        Self: std::marker::Sized,
    {
        self.http_error(message, StatusCode::INTERNAL_SERVER_ERROR)
    }
}

/// Implementation of the HTTP error converter trait
impl<T, E: Error> IntoHttpError<T> for core::result::Result<T, E> {
    fn http_error(
        self,
        message: &str,
        status_code: StatusCode,
    ) -> core::result::Result<T, actix_web::Error> {
        match self {
            Ok(val) => Ok(val),
            Err(err) => {
                error!("Caught error: {:?}", err);
                Err(InternalError::new(message.to_string(), status_code).into())
            }
        }
    }
}
