use prometheus_client::registry::Registry;

use crate::Metrics;

pub fn register(registry: &mut Registry, metrics: &Metrics) {
    registry.register(
        "wifi",
        "Current WiFi signal strength, in db",
        metrics.wifi.clone(),
    );
    registry.register(
        "pm02",
        "Particulate Matter PM2.5, in μg/m³",
        metrics.particulate_matter.clone(),
    );
    registry.register(
        "tvoc",
        "Total volatile organic compounds, in μg/m³",
        metrics.total_volatile_organic_compounds.clone(),
    );
    registry.register(
        "rco2",
        "Room CO2 value, in ppm",
        metrics.carbon_dioxide.clone(),
    );
    registry.register(
        "atmp",
        "Absolute temperature, in Celsius",
        metrics.temperature.clone(),
    );
    registry.register(
        "rhum",
        "Relative humidity, in percent",
        metrics.relative_humidity.clone(),
    );
}
