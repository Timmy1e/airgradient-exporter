FROM rust:1.58-alpine3.15 as builder
WORKDIR /usr/src/app
RUN apk add --no-cache musl-dev openssl-dev pkgconf
COPY . .
RUN cargo build --release

FROM scratch
COPY --from=builder /usr/src/app/target/release/airgradient-exporter /usr/local/bin/airgradient-exporter
CMD ["airgradient-exporter"]
