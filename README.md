# Airgradient Exporter

This is a drop in server written in Rust for the Airgradient stock sensors. Simply deploy this server, point the sensors endpoint to this server, scrape the
data using Prometheus, and graph it using Grafana.

## Container images

See: [Gitlab container registry](https://gitlab.com/Timmy1e/airgradient-exporter/container_registry) for a list of images. Images and releases are automatically
created on releases.
